import os
from flask import Flask, redirect, url_for, request, render_template, flash, jsonify
import flask
from pymongo import MongoClient
import arrow
import acp_times
import urllib.parse
import json
import config
import random
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired

import test_token
import password

from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired 

from flask_login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin,
                            confirm_login, fresh_login_required)
app = Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY


##sources,class slides, piaza resources, demo code, https://www.youtube.com/watch?v=CSHx6eCkmv0, https://www.youtube.com/watch?v=addnlzdSQs4

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)

db = client.tododb
users = client.users
tokens = client.tokens

login_manager = LoginManager()
login_manager.init_app(app)

login_manager.login_view = 'Login'
login_manager.setup_app(app)

class User(UserMixin):
    def __init__(self, name, id, active=True):
        self.name= name
        self.id = id
        self.active = active
    def is_active(self):
        return self.active

@login_manager.user_loader
def load_user(name):
    person = users.users.find_one({"username": name})
    if not person:
        return None
    return User(person["name"], person["id"])

@app.route('/api/register', methods=['GET','POST'])
def reg():
    form = regform()
    username = request.form.get('username')
    pw = request.form.get('password')

    if request.method == 'POST' and form.validate():
        index = random.randint(1,99999)
        hashword = password.hash_password(form.password.data)

        userdict = {
            "id" : index,
            "username": form.username.data,
            "password": hashword
        }
        users.users.insert_one(userdict)

        return flask.jsonify({'username': form.username.data})
    return render_template('regtest.html', form=form)

class logform(FlaskForm):
    username = StringField('Username', validators=[DataRequired(message=u'username')])
    password = PasswordField('Password', validators=[DataRequired(message=u'password')])
    remember = BooleanField('Remember Me')

class clearform(FlaskForm):
    logout = SubmitField('Log Out')

class regform(FlaskForm):
    username = StringField("username", validators=[DataRequired()])
    password = PasswordField("password", validators = [DataRequired()])
    logout = SubmitField('Logout')

@app.route('/api/token', methods=['GET','POST'])
def login():
    form = logform()
    form2 = clearform()
    if form2.logout.data:
        tokens.tokens.drop()
    if form.validate_on_submit():
        user = users.users.find_one({"username": form.username.data})
        if user == None:
            return error(Exception) #?
        pw = user['password']
        if not password.verify_password(form.password.data, pw):
            return redirect(url_for('regtest'))
        duration = 30
        if form.remember.data:#if checked, increase time 
                duration = 1337
        tokentest = test_token.generate_auth_token(duration)
        tokenjson = {"token": tokentest,}
        tokens.tokens.insert_one(tokenjson)
                  
        response = flask.jsonify(token=tokentest.decode(), time=duration)
                                
        return response

    return render_template("login.html", form=form, form2=form2)
            

@app.errorhandler(401)
def error(e):
    return render_template('401.html'), 401


@app.route('/')
@app.route("/index")
def index():
    #_items = db.tododb.find()
    #items = [item for item in _items]
    # return render_template('todo.html', items=items)
    return flask.render_template('calc.html')

@app.route('/display', methods=['POST']) #display
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]
    if items != []:
        return render_template('todo.html', items=items)
    else:
        return render_template('none.html')
@app.route('/reset', methods=['POST'])
def reset():
    db.tododb.remove({})
    return render_template('calc.html')

@app.route('/submit', methods=['POST'])
def submit():
    opentime = request.form.getlist('open')
    closetime = request.form.getlist('close')
    
    for each in range (10): #gosh I hope you dont have more than 10 controls
        if each != '':
            item_doc = {
                'opent': opentime[each],
                'closet': closetime[each]
            }
        db.tododb.insert_one(item_doc)

    return redirect(url_for('index'))

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    
    distance = request.args.get('distance',999, type=int)
    begin_date = request.args.get('begin_date',999, type=str)
    begin_time = request.args.get('begin_time',999, type=str)
    start_time_arrow = arrow.get('{}T{}'.format(begin_date, begin_time))
    start_time = start_time_arrow.isoformat() ##HERE??
    
    open_time = acp_times.open_time(km, distance, start_time)
    close_time = acp_times.close_time(km, distance, start_time)
    
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)
    

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
